/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.ejb;

import challenge.skaters.entity.SkaterUser;
import challenge.skaters.entity.Trick;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author franc
 */
@Stateless
public class SkateUserSessionBean {

    @PersistenceContext(unitName = "challenge.skaters_challenge.skaters_war_1PU")
    private EntityManager em;
    
    @EJB
    private ValidatorUtil util;
    private static final Logger LOG = Logger.getLogger(SkateUserSessionBean.class.getName());
    
    /**
     * Create a new skater user
     * @param skaterUser
     * @return 
     */
    public Response createSkateUser(SkaterUser skaterUser) {
        try {
            boolean found = em.createNamedQuery("SkaterUser.findNameExist", Boolean.class)
                    .setParameter("name", skaterUser.getSkatername())
                    .setParameter("id", skaterUser.getId() == null ? 0 : skaterUser.getId())
                    .getSingleResult();
            if (found) {
                return util.errorMessage("skater name already exist");
            }
            em.persist(skaterUser);
            return util.okMessage("new skate user created");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
    }
    
    /**
     * Delete a skater user
     * @param skaterUser
     * @return 
     */
    public Response deleteSkateUser(SkaterUser skaterUser) {
        try {
            SkaterUser currentUser;
            try {
                currentUser = em.createNamedQuery("SkaterUser.findBySkatername", SkaterUser.class)
                        .setParameter("skatername", skaterUser.getSkatername())
                        .getSingleResult();
            } catch (NoResultException e) {
                return util.errorMessage("skater name not found");
            }
            
            em.remove(currentUser);
            
            return util.okMessage("skate user deleted");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
    }
    
    /**
     * List all skater users created
     * @param limit max number of size of result
     * @return 
     */
    public Response listSkateUsers(int limit){
        try {
            List<SkaterUser> resultList = em.createNamedQuery("SkaterUser.findAll", SkaterUser.class)
                    .setMaxResults(limit == 0 ? 50 : limit)
                    .getResultList();
            for (SkaterUser skaterUser : resultList) {
                if (skaterUser.getTrickList() == null) {
                    skaterUser.setTrickList(new ArrayList<Trick>());
                } else {
                    for (Trick trick : skaterUser.getTrickList()) {
                        trick.setSkatername(trick.getSkateruser().getSkatername());
                    }
                }
            }
            return Response.ok(resultList, MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
    }
}
