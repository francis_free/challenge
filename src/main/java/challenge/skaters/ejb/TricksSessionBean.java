/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.ejb;

import challenge.skaters.entity.SkaterUser;
import challenge.skaters.entity.Trick;
import challenge.skaters.service.JsTrick;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author franc
 */
@Stateless
public class TricksSessionBean {

    @PersistenceContext(unitName = "challenge.skaters_challenge.skaters_war_1PU")
    private EntityManager em;
    @EJB
    private ValidatorUtil util;
    private static final Logger LOG = Logger.getLogger(TricksSessionBean.class.getName());
    
    /**
     * Retrieve tricks created
     * @param limit max number of size of result
     * @return 
     */
    public Response listTricks(int limit) {
        try {
            List<Trick> resultList = em.createNamedQuery("Trick.findAll", Trick.class)
                    .setMaxResults(limit)
                    .getResultList();
            for (Trick trick : resultList) {
                trick.setSkatername(trick.getSkateruser().getSkatername());
            }
            return Response.ok(resultList, MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
    }

    /**
     * Create new trick
     * @param trick
     * @return 
     */
    public Response createTrick(Trick trick) {
        try {
            SkaterUser skateruser;
            try {
                skateruser = em.createNamedQuery("SkaterUser.findBySkatername", SkaterUser.class)
                        .setParameter("skatername", trick.getSkatername())
                        .getSingleResult();
            } catch (NoResultException e) {
                return util.errorMessage("skater name not found");
            }
            trick.setSkateruser(skateruser);
            em.persist(trick);
            return util.okMessage("new trick created");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
    }

    /**
     * Create a list of tricks
     * @param tricks
     * @return 
     */
    public Response createTrickList(List<Trick> tricks) {
        try {
            for (Trick trick : tricks) {
                SkaterUser skateruser;
                try {
                    skateruser = em.createNamedQuery("SkaterUser.findBySkatername", SkaterUser.class)
                            .setParameter("skatername", trick.getSkatername())
                            .getSingleResult();
                } catch (NoResultException e) {
                    return util.errorMessage("skater name not found");
                }
                trick.setSkateruser(skateruser);
                em.persist(trick);
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
        return util.okMessage("new tricks created");
    }

    /**
     * Set a trick to favorite or un favorite
     * @param jt
     * @return 
     */
    public Response voteOnTrick(JsTrick jt) {
        try {
            Trick trick;
            try {
                trick = em.createNamedQuery("Trick.findById", Trick.class)
                        .setParameter("id", jt.getTrickid())
                        .getSingleResult();
            } catch (NoResultException e) {
                return util.errorMessage("trick not found");
            }
            SkaterUser skateruser;
            try {
                skateruser = em.createNamedQuery("SkaterUser.findBySkatername", SkaterUser.class)
                        .setParameter("skatername", jt.getSkatername())
                        .getSingleResult();
            } catch (NoResultException e) {
                return util.errorMessage("skater name not found");
            }
            if (trick.getSkateruser().getSkatername().equals(skateruser.getSkatername())) {
                trick.setSkatername(jt.getSkatername());
                trick.setFavorite(jt.getFavorite());
                return util.okMessage(String.format("%s just set trick to be  %s ", jt.getSkatername(), jt.getFavorite() ? "fovorite" : "unfovorite"));
            } else {
                return util.errorMessage(String.format("the provided trick was not created by %s unable to proceded with operation", jt.getSkatername()));
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return util.errorMessage("error occured unable to proceed");
        }
    }

}
