/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.service;

import challenge.skaters.ejb.TricksSessionBean;
import challenge.skaters.ejb.ValidatorUtil;
import challenge.skaters.entity.Trick;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author franc
 */
@Path("trick")
public class TrickResource {

    @Context
    private UriInfo context;
    @EJB
    private ValidatorUtil validatorUtil;
    @EJB
    private TricksSessionBean tsb;

    /**
     * Creates a new instance of TrickResource
     */
    public TrickResource() {
    }

    @GET
    @Path("list/{limit}")
    public Response list(@PathParam("limit") String limit) {
        int value;
        try {
            value = Integer.parseInt(limit);
        } catch (NumberFormatException e) {
            return validatorUtil.errorMessage("invalid limit value");
        }
        return tsb.listTricks(value);
    }
    
    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(String jsonstring) {
        Object obj = validatorUtil.validJson(jsonstring, Trick.class);
        if (obj instanceof Response) {
            return (Response) obj;
        } else {
            Trick trick = (Trick) obj;
            Response validate = validatorUtil.validate(trick);
            if (validate != null) {
                return validate;
            }
            return tsb.createTrick(trick);
        }
    }
    
    @POST
    @Path("create/list")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createList(List<Trick> tricks) {
        for (Trick trick : tricks) {
            Response validate = validatorUtil.validate(trick);
            if (validate != null) {
                return validate;
            }
        }
        return tsb.createTrickList(tricks);
    }
    
    @POST
    @Path("vote")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response vote(String jsonstring) {
        Object obj = validatorUtil.validJson(jsonstring, JsTrick.class);
        if (obj instanceof Response) {
            return (Response) obj;
        } else {
            JsTrick trick = (JsTrick) obj;
            Response validate = validatorUtil.validate(trick);
            if (validate != null) {
                return validate;
            }
            return tsb.voteOnTrick(trick);
        }
    }
    
}
