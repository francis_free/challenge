/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.service;

import challenge.skaters.ejb.TrimmedSize;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franc
 */
public class JsTrick {
    
    @NotNull(message = "trickid can not be null")
    private int trickid;
    @NotNull(message = "skater name can not be null")
    @Size(min = 1, max = 250, message = " skater name allowed length {min} to {max}")
    @TrimmedSize(min = 1, max = 250, message = "skater name blank space not allowed")
    private String skatername;
    @NotNull(message = "favorite can not be null")
    private boolean favorite;

    public int getTrickid() {
        return trickid;
    }

    public void setTrickid(int trickid) {
        this.trickid = trickid;
    }

    public String getSkatername() {
        return skatername;
    }

    public void setSkatername(String skatername) {
        this.skatername = skatername;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
    
    
}
