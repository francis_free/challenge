/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.service;

import challenge.skaters.ejb.SkateUserSessionBean;
import challenge.skaters.ejb.ValidatorUtil;
import challenge.skaters.entity.SkaterUser;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author franc
 */
@Path("user")
public class UserResource {

    @Context
    private UriInfo context;
    @EJB
    private ValidatorUtil validatorUtil;
    @EJB
    private SkateUserSessionBean susb;
    

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(String jsonstring) {
        Object obj = validatorUtil.validJson(jsonstring, SkaterUser.class);
        if (obj instanceof Response) {
            return (Response) obj;
        } else {
            SkaterUser user = (SkaterUser) obj;
            Response validate = validatorUtil.validate(user);
            if (validate != null) {
                return validate;
            }
            return susb.createSkateUser(user);
        }
    }
    
    @POST
    @Path("delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(String jsonstring) {
        Object obj = validatorUtil.validJson(jsonstring, SkaterUser.class);
        if (obj instanceof Response) {
            return (Response) obj;
        } else {
            SkaterUser user = (SkaterUser) obj;
            Response validate = validatorUtil.validate(user);
            if (validate != null) {
                return validate;
            }
            return susb.deleteSkateUser(user);
        }
    }
    
    @GET
    @Path("list/{limit}")
    public Response list(@PathParam("limit") String limit) {
        int value;
        try {
            value = Integer.parseInt(limit);
        } catch (NumberFormatException e) {
            return validatorUtil.errorMessage("invalid limit value");
        }
        return susb.listSkateUsers(value);
    }
}
