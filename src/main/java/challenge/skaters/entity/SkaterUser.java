/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.entity;

import challenge.skaters.ejb.TrimmedSize;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franc
 */
@Entity
@Table(name = "skateruser", catalog = "challenge_db", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"skatername"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SkaterUser.findAll", query = "SELECT s FROM SkaterUser s LEFT JOIN FETCH s.trickList ORDER BY s.id")
    , @NamedQuery(name = "SkaterUser.findById", query = "SELECT s FROM SkaterUser s WHERE s.id = :id")
    , @NamedQuery(name = "SkaterUser.findBySkatername", query = "SELECT s FROM SkaterUser s WHERE s.skatername = :skatername")
    , @NamedQuery(name = "SkaterUser.findNameExist", query = "SELECT CASE WHEN(COUNT(s.id) > 0L) THEN TRUE ELSE FALSE END FROM SkaterUser s WHERE s.skatername = :name AND s.id != :id")})
public class SkaterUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull(message = "skater name can not be null")
    @Size(min = 1, max = 100, message="skater name allowed length {min} to {max}")
    @TrimmedSize(min = 1, max = 250, message = "skater name blank space not allowed")
    @Column(name = "skatername", nullable = false, length = 100)
    private String skatername;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "skateruser")
    private List<Trick> trickList;

    public SkaterUser() {
    }

    public SkaterUser(Integer id) {
        this.id = id;
    }

    public SkaterUser(Integer id, String skatername) {
        this.id = id;
        this.skatername = skatername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSkatername() {
        return skatername;
    }

    public void setSkatername(String skatername) {
        this.skatername = skatername;
    }

    
    public List<Trick> getTrickList() {
        return trickList;
    }

    public void setTrickList(List<Trick> trickList) {
        this.trickList = trickList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SkaterUser)) {
            return false;
        }
        SkaterUser other = (SkaterUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "challenge.skaters.entity.SkaterUser[ id=" + id + " ]";
    }
    
}
