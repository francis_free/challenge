/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challenge.skaters.entity;

import challenge.skaters.ejb.TrimmedSize;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franc
 */
@Entity
@Table(name = "trick", catalog = "challenge_db", schema = "public")
@XmlRootElement
@JsonIgnoreProperties(value = { "skateruser"})
@NamedQueries({
    @NamedQuery(name = "Trick.findAll", query = "SELECT t FROM Trick t JOIN FETCH t.skateruser ORDER BY t.id")
    , @NamedQuery(name = "Trick.findById", query = "SELECT t FROM Trick t WHERE t.id = :id")
    , @NamedQuery(name = "Trick.findByTrickckname", query = "SELECT t FROM Trick t WHERE t.trickname = :trickname")
    , @NamedQuery(name = "Trick.findByDescription", query = "SELECT t FROM Trick t WHERE t.description = :description")
    , @NamedQuery(name = "Trick.findByLocations", query = "SELECT t FROM Trick t WHERE t.locations = :locations")
    , @NamedQuery(name = "Trick.findByFavorite", query = "SELECT t FROM Trick t WHERE t.favorite = :favorite")})
public class Trick implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull( message = "trick name can not be null")
    @Size(min = 1, max = 250, message = "trick name allowed length {min} to {max}")
    @TrimmedSize(min = 1, max = 250, message = "trick name blank space not allowed")
    @Column(name = "trickname", nullable = false, length = 250)
    private String trickname;
    @Basic(optional = false)
    @NotNull( message = "description can not be null")
    @Size(min = 1, max = 250, message = "description allowed length {min} to {max}")
    @TrimmedSize(min = 1, max = 250, message = "description blank space not allowed")
    @Column(name = "description", nullable = false, length = 250)
    private String description;
    @NotNull( message = "locations can not be null")
    @Size(min = 1, max = 250, message = "location allowed length {min} to {max}")
    @TrimmedSize(min = 1, max = 250, message = "location blank space not allowed")
    @Column(name = "locations", nullable = false, length = 250)
    private String locations;
    @Column(name = "favorite")
    private Boolean favorite;
    @JoinColumn(name = "skateruser", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private SkaterUser skateruser;
    @Transient
    @NotNull( message = "skater name can not be null")
    @Size(min = 1, max = 250, message = " skater name allowed length {min} to {max}")
    private String skatername;

    public Trick() {
    }

    public Trick(Integer id) {
        this.id = id;
    }

    public Trick(Integer id, String trickname, String description, String locations) {
        this.id = id;
        this.trickname = trickname;
        this.description = description;
        this.locations = locations;
    }
    
    @PrePersist
    private void initValues(){
        if(this.favorite == null){
            this.favorite = Boolean.FALSE;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTrickname() {
        return trickname;
    }

    public void setTrickname(String trickname) {
        this.trickname = trickname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public SkaterUser getSkateruser() {
        return skateruser;
    }

    public void setSkateruser(SkaterUser skateruser) {
        this.skateruser = skateruser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trick)) {
            return false;
        }
        Trick other = (Trick) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "challenge.skaters.entity.Trick[ id=" + id + " ]";
    }

    public String getSkatername() {
        return skatername;
    }

    public void setSkatername(String skatername) {
        this.skatername = skatername;
    }
    
}
